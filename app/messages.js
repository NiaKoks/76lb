const express = require('express');
const nanoid = require('nanoid');
const db = require('../fileDb');

const router = express.Router();


router.get('/', (req, res) => {
    console.log(req.query.datetime);
    if(req.query.datetime) {
        console.log(req.query.datetime);
        res.send(db.getByDate(req.query.datetime));
    } else {
        res.send(db.getItems())
    }
});

router.post('/', (req, res) => {
 if(req.body.author !== '' && req.body.message !== ''){


     const  date=new Date().toISOString();
     const message ={
         date: date,
         author: req.body.author,
         message: req.body.message,
         id: nanoid()
     };

     db.addItem(message);
     res.send({message: 'OK'})
 }else {
     res.status(500).send({error: 'Author and message mustbe present in the request!'})
 }
});



module.exports = router;
